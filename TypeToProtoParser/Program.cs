﻿using System;
using System.Reflection;
using System.Linq;

namespace TypeToProtoParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new Config();
            try 
            {
                var assembly = Assembly.LoadFrom(config.AssemblyFilePath);
                var types = assembly.GetTypes();
                var typesToBeParsed = config.TypeNamesToBeParsed
                    .Select(n => types.FirstOrDefault(t => t.Name.Equals(n)))
                    .Where(type => type != null).ToList();
                Parser.ParseTypesToFile(typesToBeParsed, config.OutputFileName + ".proto", config.OutputFileName);
            }
            catch (Exception e) 
            {
                Console.WriteLine($"Exception:\r\n{e}" +
                    $"Stack trace:\r\n{e.StackTrace}");
            }
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
