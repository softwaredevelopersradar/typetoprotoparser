﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TypeToProtoParser
{
    public class Config
    {
        public string AssemblyFilePath { get; set; }
        public string OutputFileName { get; set; }

        public List<string> TypeNamesToBeParsed { get; set; }

        public Config(string configFileName = "config.txt") 
        {
            TypeNamesToBeParsed = new List<string>(50);
            try 
            {
                var lines = File.ReadAllLines(configFileName);
                AssemblyFilePath = lines[0];
                OutputFileName = lines[1];
                for (int i = 2; i < lines.Length; i++)
                {
                    TypeNamesToBeParsed.Add(lines[i]);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"Exception occured:\r\n{e}");
            }
        }
    }
}
